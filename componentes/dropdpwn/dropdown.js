function init() {

    // Dropdown
    // --------------------------------------------------------
    var dropdown01 = document.querySelector('.dropdown__btn01');
  
    dropdown01.onclick = function(event) {
      var dropdown01 = this;
      if (dropdown01.hasAttribute('aria-expanded') && dropdown01.getAttribute('aria-expanded') === 'false') {
        dropdown01.setAttribute('aria-expanded', 'true');
      } else {
        dropdown01.setAttribute('aria-expanded', 'false');
      }
    }

    var dropdown02 = document.querySelector('.dropdown__btn02');
  
    dropdown02.onclick = function(event) {
      var dropdown02 = this;
      if (dropdown02.hasAttribute('aria-expanded') && dropdown02.getAttribute('aria-expanded') === 'false') {
        dropdown02.setAttribute('aria-expanded', 'true');
      } else {
        dropdown02.setAttribute('aria-expanded', 'false');
      }
    }

    var dropdown03 = document.querySelector('.dropdown__btn03');
  
    dropdown03.onclick = function(event) {
      var dropdown03 = this;
      if (dropdown03.hasAttribute('aria-expanded') && dropdown03.getAttribute('aria-expanded') === 'false') {
        dropdown03.setAttribute('aria-expanded', 'true');
      } else {
        dropdown03.setAttribute('aria-expanded', 'false');
      }
    }
  
  
  
  };
  
  window.onload = init;